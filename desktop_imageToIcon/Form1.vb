﻿Option Strict On

Public Class Form1
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Me.DesignMode Then
            Return
        End If
        Dim aCommand = Command()

        If aCommand <> "" Then
            Dim aInfo = Strings.Split(aCommand & "*******", "*")
            Dim aPFN = aInfo(0)
            Dim aUkuran As Integer
            If Integer.TryParse(aPFN(1), aUkuran) = False Then
                aUkuran = 256
            End If
            alghasjgh(aPFN, aUkuran)
            Environment.Exit(0)
        End If
    End Sub

    Private Sub alghasjgh(ByVal aPFN As String, ByVal iUkuran As Integer)
        Dim aUkuran = iUkuran
        Label1.Text = "Ok converting.."
        Application.DoEvents()
        'save standard windows icon, resized

        If aPFN = "" Then
            aPFN = "C:\Users\livan64\Desktop\home sheep home lost in london.png"
        End If

        Dim aBitmap As Bitmap = New Bitmap(aUkuran, aUkuran, Imaging.PixelFormat.Format24bppRgb) ' CType(Bitmap.FromFile(aPFN), Bitmap)
        Dim aIcon = Icon.FromHandle(aBitmap.GetHicon)
        Dim aPFNiconTemp = IO.Path.ChangeExtension(aPFN, ".ico")
        Dim aFileStream = New IO.FileStream(aPFNiconTemp, IO.FileMode.OpenOrCreate)
        aIcon.Save(aFileStream)
        aFileStream.Close()
        aIcon.Dispose()

        '==> replace with 32bit image
        Dim aTemp = New livanImageResizer.myImage
        aTemp.setImage(aPFN)
        Dim aSize = DevWilson.ImageHeader.GetDimensions(aPFN)
        Dim aPersen = CSng(aUkuran / aSize.Width) * 100
        Dim aBitmapBaru = aTemp.getScaledImage(aPersen)
        aTemp.Dispose()
        Dim aIconHandle = aBitmapBaru.GetHicon()
        Dim aIconEx As New vbAccelerator.Components.Win32.IconEx(aPFNiconTemp)
        aIconEx.Items.RemoveAt(0)
        Dim IconDeviceImage As New vbAccelerator.Components.Win32.IconDeviceImage(New Size(aUkuran, aUkuran), ColorDepth.Depth24Bit)
        'gets bitmap of (assumed) 32 x 32 image in picturebox, sets it to IconImage
        IconDeviceImage.IconImage = aBitmapBaru
        'adds icondevicimage to the icon file
        aIconEx.Items.Add(IconDeviceImage)
        'saves icon 
        Label1.Text = "saving.."
        Application.DoEvents()
        aIconEx.Save(aPFN & ".ico")
        IO.File.Delete(aPFNiconTemp)
    End Sub

    'Private Sub Label1_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Label1.DragDrop
    '    Try
    '        Dim aFileDrop = CType(e.Data.GetData("FileDrop", True), String())
    '        For Each aFile In aFileDrop
    '            alghasjgh(aFile)
    '            Exit For
    '        Next
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    '    e.Effect = DragDropEffects.None
    '    Environment.Exit(0)
    'End Sub

    Private Sub Label1_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Label1.DragEnter
        If e.Data.GetDataPresent("FileDrop") Then
            e.Effect = DragDropEffects.Copy
        End If
    End Sub
End Class
